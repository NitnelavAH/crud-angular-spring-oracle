import { Component, OnInit } from '@angular/core';
import { PersonasService } from '../../services/personas.service';
import { Persona } from '../../models/persona';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss']
})
export class EditarComponent implements OnInit {

  persona: Persona = {
    amaterno: '',
    apaterno: '',
    nombre: ''
  }

  constructor(
    private personasService: PersonasService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      let id = params.id;
      this.getPersona(id);
    })
  }


  getPersona(id: number){
    this.personasService.getPersona(id).subscribe(
      res=>{
        if(res){
          this.persona=res;
        }
      }
    )
  }

  onSubmit(){
    this.personasService.newPersona(this.persona).subscribe(
      res=>{
        if(res){
          alert(this.persona.nombre + " Fue actualizad@");
          this.router.navigate(["listar"]);
        }
      }
    )
  }

}
