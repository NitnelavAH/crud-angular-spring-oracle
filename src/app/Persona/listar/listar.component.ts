import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from 'src/app/models/persona';
import { PersonasService } from '../../services/personas.service';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {


  personas: Persona[] = [];

  constructor(
    private personasService: PersonasService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getPersonas();
  }

  getPersonas(){
    this.personasService.getPersonas().subscribe(data => {
      this.personas = data;
      this.personas.sort((a,b) => a.apaterno.localeCompare(b.apaterno));
    });
  }

  nuevaPersona(){
    this.router.navigate(["nuevo"]);
  }

  eliminarPersona(persona: Persona){
      let bandera = confirm("Eliminar a "+persona.nombre+" "+persona.apaterno+" "+persona.amaterno+"?");
      if(bandera){
        this.personasService.eliminarPersona(persona.id).subscribe(res => {
          if(res){
            alert("Se ha eliminado a "+persona.nombre);
            this.getPersonas();
          } else {
            alert("No se pudo eliminar a "+persona.nombre);
          }
        });
      }
  }

}
