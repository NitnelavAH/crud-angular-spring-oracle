import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from '../../models/persona';
import { PersonasService } from '../../services/personas.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  persona: Persona = {
    amaterno: '',
    apaterno: '',
    nombre: ''
  }

  constructor(
    private personasService: PersonasService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.personasService.newPersona(this.persona).subscribe(
      res=>{
        if(res){
          alert(this.persona.nombre + " Fue registrad@");
          this.router.navigate(["listar"]);
        }
      }
    )
  }
}
