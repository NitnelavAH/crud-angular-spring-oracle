import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarComponent } from './Persona/listar/listar.component';
import { AddComponent } from './Persona/add/add.component';
import { EditarComponent } from './Persona/editar/editar.component';


const routes: Routes = [
  { path: '', redirectTo: 'listar', pathMatch: 'full' },
  {path: 'listar', component: ListarComponent},
  {path: 'nuevo', component: AddComponent},
  {path: 'edit/:id', component: EditarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
