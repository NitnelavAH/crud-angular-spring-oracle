import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Persona } from '../models/persona';
import { Observable } from 'rxjs';

const URL = 'http://localhost:8080/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  personas: Persona[] = [];

  constructor(
    private http: HttpClient
  ) { }

  getPersonas():Observable<Persona[]>{
    return this.http.get<Persona[]>(URL);
  }

  getPersona(id: number){
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get<Persona>(URL+"/"+id,{headers: headers});
  }
  
  newPersona(persona: Persona):Observable<Persona>{
    let params = JSON.stringify(persona);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<Persona>(URL, params, {headers: headers});

  }

  eliminarPersona(id: number){
    return this.http.delete(URL + '/'+id,{responseType: 'text'});
  }
}
